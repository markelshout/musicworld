# Music World: an educational workspace for music teachers

This is an educational application for the
[*Developing Musicianship*] brand (http://www.developingmusicianship.com/)
by [Mark Elshout](http://www.developingmusicianship.com/).